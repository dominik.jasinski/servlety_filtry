package pl.djasinski.filters;

import javax.servlet.*;
import java.io.IOException;
import java.util.Map;

/**
 * Zad_12_filtry.pdf
 */

public class FirstFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("BEFORE FIRST FILTER");
        request.setAttribute("secondFilterAttr", "Second Filter Attr");

        if (shouldBlock(request)) {
            return;
        }

        filterChain.doFilter(request, response);
        System.out.println("AFTER FIRST FILTER");
    }

    private boolean shouldBlock(ServletRequest request) {
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            for (String value : entry.getValue()) {
                if ("blokuj".equalsIgnoreCase(value)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public void destroy() {
        System.out.println("############## Destroy :" + getClass().getSimpleName());
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("############## Init :" + getClass().getSimpleName());
    }
}



