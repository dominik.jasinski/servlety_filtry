package pl.djasinski.filters;

import javax.servlet.*;
import java.io.IOException;

/**
 * Zad_12_filtry.pdf
 */

public class SecondFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("BEFORE SECOND FILTER");
        request.setAttribute("firstFilterAttr", "FIRST Filter Attr");
        filterChain.doFilter(request, response);
        System.out.println("AFTER SECOND FILTER");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("############## Init :" + getClass().getSimpleName());
    }

    @Override
    public void destroy() {
        System.out.println("############## Destroy :" + getClass().getSimpleName());
    }
}
