package pl.djasinski.servlets;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Zad_11_servlety_ciasteczka.pdf
 */

@WebServlet(
        name = "CookieServlet",
        value = "/cookie"
)
public class CookieServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        resp.setCharacterEncoding("UTF-8");

        String query = req.getParameter("query");

        // Ustawiamy ciasteczko w przypadku gdy użytkownik przekaże w parametr o nazwie query
        if (query != null && !query.isEmpty()) {
            Cookie cookie = new Cookie("query", query);
            cookie.setMaxAge(60 * 60);
            resp.addCookie(cookie);
        }

        PrintWriter pw = resp.getWriter();

        pw.println("<html>");
        pw.println("<body>");
        pw.println("<h1>Cookies</h1>");

        if (req.getCookies() != null && req.getCookies().length > 0) {
            pw.println(" <table border=2>");
            pw.println("  <thead>" +
                    "    <tr>" +
                    "      <th>Cookie name</th>" +
                    "      <th>Cookie value</th>" +
                    "    </tr>" +
                    "  </thead>");

            for (Cookie cookie : req.getCookies()) {
                pw.println(
                        "<tr><td>" + cookie.getName() + "</td>" +
                                "<td>" + cookie.getValue() + "</td></tr>"
                );
            }

            pw.println("</table>");
        }

        pw.println("<body>");
        pw.println("</body>");
        pw.println("</html>");
    }
}
