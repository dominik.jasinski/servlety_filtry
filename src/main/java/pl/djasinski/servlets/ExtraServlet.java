package pl.djasinski.servlets;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Zad_9_servlety_config_param.pdf
 */


@WebServlet(
        name = "ExtraServlet",
        urlPatterns = {"/extra", "/Extra"},
        initParams = {
                @WebInitParam(name = "userName", value = "not provided"),
                @WebInitParam(name = "email", value = "not provided")
        }
)
public class ExtraServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter pw = resp.getWriter();

        String userName = getRequestParameter(req, "userName");
        String email = getRequestParameter(req, "email");

        pw.println("<html>");
        pw.println("<body>");
        pw.println("<p><strong>User Name:</strong> " + userName +
                "<br><strong>email address: </strong>" + email + "</p>");
        pw.println("</body>");
        pw.println("</html>");
    }

    /**
     * Metoda mająca na celu pobranie wartości z parametrów userName oraz email w przypadku gdy użytkownik
     * ich nie przekaże wartości pobierane są z parametrów initialnych servletu
     */
    private String getRequestParameter(HttpServletRequest request, String name) {
        String param = request.getParameter(name);

        return param != null ? param : getInitParameter(name);
    }
}
