package pl.djasinski.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorldServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter pw = resp.getWriter();

        String paramA = req.getParameter("paramA");

        pw.println("<html>");
        pw.println("<body>");
        pw.println("<h1>Hello </h1>");
        pw.println("<p>World !</p>");
        pw.println("<p>paramA: " + paramA + " paramA: " + paramA + "</p>");
        pw.println("<body>");
        pw.println("<p>session: " + req.getSession().isNew() + "</p>");
        pw.println("</body>");
        pw.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter pw = resp.getWriter();

        String query = req.getParameter("query");
        String page = req.getParameter("page");

        pw.println("<html>");
        pw.println("<body>");
        pw.println("<h1>Hello </h1>");
        pw.println("<p>World !</p>");
        pw.println("<p>query: " + query + " page: " + page + "</p>");
        pw.println("<body>");
        pw.println("</body>");
        pw.println("</html>");
    }
}
