package pl.djasinski.servlets;


import pl.djasinski.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Zad_14_Servlety+JSP.pdf
 *
 * Servlet testujący przekazywanie i wyświetlanie atrybutów w widokach JSP
 */

@WebServlet(value = "/jspTest")
public class JSPServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        resp.setCharacterEncoding("UTF-8");

        User user = new User();
        user.setName("Antoni");

        req.setAttribute("userObject", user);

        req.getRequestDispatcher("views/jspTest.jsp").forward(req, resp);
    }
}
