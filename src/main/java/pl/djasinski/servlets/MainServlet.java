package pl.djasinski.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter pw = resp.getWriter();

        pw.println("<html>");
        pw.println("<body>");
        pw.println("<h1>Hello ąę ;|</h1>");
        pw.println("<p>World !</p>");
        pw.println("<body>");
        pw.println("</body>");
        pw.println("</html>");

        RequestDispatcher rd = req.getRequestDispatcher("index.jsp");

        rd.include(req, resp);
    }
}
