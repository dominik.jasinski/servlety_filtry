package pl.djasinski.servlets;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Zad_13_przekierowania.pdf
 */


@WebServlet(
        name = "RedirectServlet",
        value = "/redirect"
)
public class RedirectServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        resp.setCharacterEncoding("UTF-8");

        Boolean shouldRedirect = Boolean.valueOf(req.getParameter("redirect"));
        Boolean shouldInclude = Boolean.valueOf(req.getParameter("include"));

        PrintWriter pw = resp.getWriter();

        pw.println("<html>");
        pw.println("<body>");
        pw.println("<h1>RedirectServlet</h1>");
        pw.println("<body>");
        pw.println("</body>");
        pw.println("</html>");

        if (shouldRedirect) {
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        } else if (shouldInclude) {
            req.getRequestDispatcher("index.jsp").include(req, resp);
        }
    }
}
