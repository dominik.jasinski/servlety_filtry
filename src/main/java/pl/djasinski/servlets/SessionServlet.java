package pl.djasinski.servlets;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * Zad_10_servlety_sesja.pdf
 */

public class SessionServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        resp.setCharacterEncoding("UTF-8");

        String newArticle = req.getParameter("towar");

        HttpSession session = req.getSession();
        Set<String> cart = (Set<String>) session.getAttribute("koszyk");

        if (cart == null) {
            cart = new HashSet<>();
            session.setAttribute("koszyk", cart);
        }

        if (newArticle != null && !newArticle.isEmpty()) {
            cart.add(newArticle);
        }

        PrintWriter pw = resp.getWriter();

        pw.println("<html>");
        pw.println("<body>");
        pw.println("<h1>Article list</h1>");

        int i = 1;
        for (String article : cart) {
            pw.println(i + ") " + article + "<br>");
            i++;
        }

        pw.println("<body>");
        pw.println("</body>");
        pw.println("</html>");
    }
}
