package pl.djasinski.servlets;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

@WebServlet(
        name = "SumujServlet",
        value = {"/sumuj"}
)
public class SumujServlet extends HttpServlet {

    private static final String PARAM_TOTAL_SUM = "totalSum";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html; charset=utf-8");
        resp.setCharacterEncoding("UTF-8");

        double requestSum = sumNumberParams(req);
        double totalSum = updateTotalSum(req, requestSum);

        PrintWriter pw = resp.getWriter();

        pw.println("<html>");
        pw.println("<body>");
        pw.println("<h1>Article list</h1>");
        pw.println("<p>requestSum : " + requestSum + "</p>");
        pw.println("<p>totalSumt : " + totalSum + "</p>");

        pw.println("<body>");
        pw.println("</body>");
        pw.println("</html>");
    }

    private double sumNumberParams(HttpServletRequest req) {
        double requestSum = 0;

        for (Map.Entry<String, String[]> entry : req.getParameterMap().entrySet()) {
            for (String value : entry.getValue()) {
                try {
                    requestSum = requestSum + Double.valueOf(value);;
                } catch (NumberFormatException ignored) {
                    // ignore
                }
            }
        }

        return requestSum;
    }

    private double updateTotalSum(HttpServletRequest req, double requestSum) {
        HttpSession session = req.getSession();
        Double totalSum = (Double) session.getAttribute(PARAM_TOTAL_SUM);

        if (totalSum == null) {
            totalSum = 0d;
        }

        totalSum += requestSum;
        totalSum = totalSum + requestSum;

        session.setAttribute(PARAM_TOTAL_SUM, totalSum);

        return totalSum;
    }
}
