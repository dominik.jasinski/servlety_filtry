<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<body>
<h3>Cookie Value</h3>

    <%
        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                out.print(cookie.getName() + " - " + cookie.getValue() + "<br>");
            }
        }
    %>

    <%
        Cookie cookie = new Cookie("bColor", "red");
        cookie.setMaxAge(60*60);
        response.addCookie(cookie);
    %>

</body>
</html>
