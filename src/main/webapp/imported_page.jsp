<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<div>
    <h2>Imported Page!</h2>
    <p>pageScopeVariable: ${pageScopeVariable}</p>
    <p>requestScopeVariable: ${requestScopeVariable}</p>
    <p>sessionScopeVariable: ${sessionScopeVariable}</p>
    <p>applicationScopeVariable: ${applicationScopeVariable}</p>
</div>


