<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ page import="java.time.LocalDate,java.time.LocalDateTime, java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Date" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <%=  "Hello world !" %>
            <hr>
            <div id="WYRAZENIA">
                <h3>1. WYRAZENIA</h3>
                <% out.println("Hello world!"); %>
                <br>
                <% out.println(new Date()); %>
                <br>
                <%=  LocalDate.of(LocalDate.now().getYear(), 5, 1) %>
                <br>
                <%=  LocalDate.now() %>
                <br>
                <%--    @return the comparator value, negative if less, positive if greater--%>
                <%=
                LocalDate.of(LocalDate.now().getYear(), 5, 1).compareTo(LocalDate.now()) < 0 ?
                        "Już po wszystkim :(" :
                        "Jeszcze wszystko przed nami :)"
                %>
            </div>
            <hr>
            <div id="SKRYPTLETY">
                <h3>2. SKRYPTLETY</h3>
                <%
                    System.out.println("Evaluating date now");
                    Date date = new Date();
                %>
                Hello! The time is now <%= date %>
                <br>
                Jaka jest pora dnia? -
                <%
                    int hour = LocalDateTime.now().getHour();

                    if (hour > 12) {
                        out.print("Już po południu!");
                    } else {
                        out.print("jeszcze wcześnie!");
                    }
                %>
                <br>
                <%
                    for (int i = 0; i < 10; i++)
                        out.println("<br> Kod w języku Java! " + i);
                %>

                <h3>Mieszanie skryptletów z HTMLem</h3>
                <table border="2">
                    <%
                        int n = 10;
                        for (int i = 0; i < n; i++) {
                    %>
                    <tr>
                        <td>Number</td>
                        <td><%= i + 1 %>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
                <br>
                <%
                    int h = LocalDateTime.now().getHour();

                    if (h > 16) {
                %>
                <p>Hello!</p>
                <%
                } else {
                %>
                <p>Goodbye</p>
                <%
                    }
                %>
                <br>
            </div>
            <hr>
            <div id="DYREKTYWY">
                <h3>3. DYREKTYWY</h3>

                Going to @ include .jsp file <BR>


                <%@ include file="included_content.jsp" %>
            </div>
            <hr>
            <div id="PREDEFINIOWANE">
                <h3>PREDEFINIOWANE</h3>
                getParameter form request : <%=  request.getParameter("paramA")%>
                <br>
                Info <%=  getServletInfo()%>
                <br>
                <%=  response%>
                <br>
                Session start: <%=  new Date(session.getCreationTime())%>
                <br>
                Application is run on <%=  application.getServerInfo()%>
                <br>
                Default servlet name <%=  config.getServletName()%>
                <br>
                <%=  pageContext%>
                <br>
                <%=  page%>
                <br>
                <% out.println(request.getRemoteAddr());%>
            </div>
            <hr>
            <div id="DEKLARACJE">
                <h3>4. DEKLARACJE</h3>

                <%!
                    private LocalDateTime currentDate = LocalDateTime.now();

                    String whatTimeIsIt() {
                        return "Jest godzina: " + currentDate.getHour() + ":" + currentDate.getMinute();
                    }
                %>
                Hello!  <%= whatTimeIsIt() %>
                <div id="COMMENTS">
                    <h3>4. i komentarze</h3>
                    <!--To jest komentarz html-->
                    <%-- To jest komentarz jsp --%>
                </div>
            </div>
            <hr>
            <div id="EL">
                <h3>5. EL</h3>
                <p>Przekazany użytkownik w parametrze 'userName' : ${param.userName}</p>
                <p>Przekazany użytkownik w parametrze 'user-name' : ${param['user-name']}</p>
            </div>
            <hr>
            <div id="ZNACZNIKI_AKCJI">
                <h3>6. Znaczniki akcji</h3>

                <%--                <jsp:forward page="/included_content.jsp">--%>
                <%--                    <jsp:param name="czyAlaMaKota" value="Tak ala ma kota"/>--%>
                <%--                    <jsp:param name="includedUserName" value="${param.userName}"/>--%>
                <%--                </jsp:forward>--%>

                <jsp:include page="/included_content.jsp">
                    <jsp:param name="czyAlaMaKota" value="Tak ala ma kota"/>
                    <jsp:param name="includedUserName" value="${param.userName}"/>
                </jsp:include>
            </div>
            <hr>
            <div id="SCOPE">
                <h3>7. SCOPE</h3>

                <c:set var="appVariable" value="Joanny - app scope" scope="application"/>
                <c:set var="pageScopeVariable" value="Joanny - page scope" scope="page"/>
                <p>app_variable: ${appVariable}</p>
                <p>pageScopeVariable: ${pageScopeVariable}</p>

            </div>
            <div id="JSTL">
                <h3>8. JSTL</h3>
                <c:set var="formatDate" value="<%=new Date()%>"/>
                <c:set var="formatNumber" value="<%=new Integer(20)%>"/>

                <c:set var="collections" value='<%=new ArrayList<String>(
                            Arrays.asList("Geeks",
                                         "for",
                                        "Geeks"))%>'
                />

                <c:forEach var="item" items="${collections}">
                    ${item} <br>
                </c:forEach>
            </div>


            <hr>

            <fmt:formatDate value="${formatDate}" pattern="yyyy-MMM-dd"/>

            <c:set var="balance" value="120000.2309"/>
            <p>Currency in USA :
                <%--    <fmt:setLocale value="en_US"/>--%>
                <fmt:setLocale value="de_DE"/>
                <fmt:formatNumber value="${balance}" type="currency"/>
            </p>


            <fmt:formatNumber value="${balance}" type="currency" minFractionDigits="2" maxFractionDigits="2"
                              currencyCode="PLN"/>

            <fmt:formatDate value="${formatDate}" type="both"/>

            <hr>
            <div>
                <h3>Przekierowanie</h3>
                <div class="col-md-12">
                    <div style="cursor:pointer;" onclick="location.href = '<c:url value="/included_content.jsp"/>';">
                        <img src="http://icons.iconarchive.com/icons/oxygen-icons.org/oxygen/256/Actions-view-calendar-list-icon.png"
                             width="140" height="140">
                        <a class="btn btn-success " href='<c:url value="/included_content.jsp" />' role="button">Lista
                            &raquo;</a>
                    </div>
                </div>
            </div>
            <hr>
            <div>
                <h3>JSTL</h3>
                <h3>set</h3>
                <c:set var="user" value="Damian"/>
                ${user}
                <h3>catch</h3>
                <c:catch var="exception">
                    <%=20 / 0%>
                </c:catch>
                <p>Error message: ${exception}</p>

                <h3>choose</h3>
                <c:choose>
                    <c:when test="${param.name eq 'Anna'}">
                        Cześć Ania:)
                    </c:when>
                    <c:when test="${param.name eq 'Jan'}">
                        Cześć Janek!
                    </c:when>
                    <c:otherwise>
                        <p>Nie ma takiej osoby na liście gości!</p>
                    </c:otherwise>
                </c:choose>

                <h3>import</h3>
                <div style="border: solid 2px red; margin: 0 20px">
                    <c:import url="http://www.google.pl"></c:import>
                </div>

                <c:import var="externalSite" url="http://www.google.pl" scope="request"></c:import>

                <div style="border: solid 2px red; margin: 0 20px">
                    ${externalSite}
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>