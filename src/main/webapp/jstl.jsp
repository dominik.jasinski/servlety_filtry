<%@ page import="pl.djasinski.model.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<html>
<body>
<h3>JSTL</h3>

<div style="border: solid 2px red; margin: 0 20px">

    <c:set var="userName" value="Kamil Nowak"/>

    <c:out value="${userName}" default="zastępcze imie"/>
    <c:out value="<"/>

</div>

<h3>escapeXml</h3>

<div style="border: solid 2px red; margin: 0 20px">

    <c:set var="userName" value="Kamil Nowak"/>

</div>

<hr>
<h3>if</h3>

<c:if test='${param.userName eq "Jan"}'>
    <p>Witaj w tajnej stronie tylko dla Janów</p>
</c:if>

<hr>

<c:set var="user" value="<%=new User()%>"/>
<c:set target="${user}" property="name" value="Damian"/>

UserName ${user.name}

<c:catch var="exception">
    <%=20 / 0%>
</c:catch>
<p>Error message: ${exception}</p>


<c:url value="/lista">
    <c:param name="page" value="3"/>
    <c:param name="phrase" value="allegro"/>
</c:url>

<c:set var="array" value='<%=new ArrayList<>(
                Arrays.asList("Geeks",
                        "for",
                        "Geeks"))%>'/>


${fn:length(array)}


</body>
</html>
