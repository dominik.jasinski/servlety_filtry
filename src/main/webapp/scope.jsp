<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<body>
<h3>import</h3>
<div style="border: solid 2px red; margin: 0 20px">
    <c:set var="pageScopeVariable" value="PAGE_SCOPE" scope="page"/>
    <c:set var="requestScopeVariable" value="REQUEST_SCOPE" scope="request"/>
    <c:set var="sessionScopeVariable" value="SESSION_SCOPE" scope="session"/>
    <c:set var="applicationScopeVariable" value="APPLICATION_SCOPE" scope="application"/>

    <p>pageScopeVariable: ${pageScopeVariable}</p>
    <p>requestScopeVariable: ${requestScopeVariable}</p>
    <p>sessionScopeVariable: ${sessionScopeVariable}</p>
    <p>applicationScopeVariable: ${applicationScopeVariable}</p>

    <c:import url="imported_page.jsp"></c:import>
</div>
</body>
</html>






