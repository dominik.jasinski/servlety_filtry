<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url value="extra" var="mojUrl">
    <c:param name="page" value="10"/>
    <c:param name="page" value="12"/>
</c:url>

<html>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>


<div class="row col-md-12">

    <%

        Cookie cookie = new Cookie("bColor", "red");
        response.addCookie(cookie);
    %>
    <form action="${mojUrl}" method="post">
        <div class="form-group controls">
            <label for="query">query</label>
            <input type="text" id="query" name="query"/>
        </div>

        <div class="form-group controls">
            <label for="page">page</label>
            <input type="text" id="page" name="page"/>
        </div>

        <div class="form-group controls">
            <select>
                <option value="desc" label="desc">malejąco</option>
                <option value="asc" label="asc">rosnąco</option>
            </select>
        </div>

        <input class="btn btn-success" type="submit" value="Submit">
    </form>
</div>


<div class="form-group">
    <div class="controls">
        <label for="page">page</label>
        <input type="text" id="sf" name="page" class="form-control">
    </div>
</div>


</body>
</html>






