<%@ page import="java.util.Iterator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<body>
<h3>import</h3>
<div style="border: solid 2px red; margin: 0 20px">

    <p>session: ${sessionScope}</p>


    <jsp:useBean id="koszyk" class="java.util.HashSet" scope="session" />


    <% String t = request.getParameter("towar");
        if (t != null) {
            koszyk.add(t);
        }
    %>

    <% for (Iterator i = koszyk.iterator(); i.hasNext(); ) {
        out.println("<li>" + i.next() + "</li>");
    }
    %>

</div>
</body>
</html>