<%@ page import="pl.djasinski.model.User" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="hi" value="Hello"/>
<%--<jsp:useBean id="myBean" class="pl.djasinski.model.User" /> <br>--%>
<c:set var="myBean" value="<%=new User()%>"/>
<c:set target="${pageScope.myBean}" property="name" value="Karol"/>


<html>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <%--           --%>
            <%--            imie usera: ${pageScope.myBean.name} <br>--%>
            session: ${myBean.name} <br>

            <%--            pageScope: ${pageScope.hi} <br>--%>
            <%--            getOrDefault: ${pageScope.getOrDefault("hii", "nie ma takiego czegos")} <br>--%>

        </div>
    </div>
</div>

</body>
</html>